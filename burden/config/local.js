/* Local configurations.
 */

module.exports = {
  label: process.env.APPNAME || "YOURAPPNAME",
  stage: process.env.ENVIRONMENT || "devel",
}
