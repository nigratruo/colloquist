module.exports = [
  /*
   * Each configuration has shape of Scenario protocol.
   *
   */
  // Call story/sample
  {
    story: 'sample',
    premise: {
      param_01: 123
    }
  }
  // Array means pararel execution
  //["DoSomethin01", "DoAnotherthin02"]
];
